import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Layout from '../containers/Layout'
import Consultas from '../containers/Consultas'
import Pacientes from '../containers/Pacientes'
import Citas from '../containers/Citas'
import NotFound from '../pages/NotFound'
import Home from '../pages/Home'
import '../styles/global.scss'

const App = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/citas" element={<Citas />} />
          <Route exact path="/consultas" element={<Consultas />} />
          <Route exact path="/pacientes" element={<Pacientes />} />
          <Route exact path="*" element={<NotFound />} />
        </Routes>
      </Layout>
    </BrowserRouter>
  )
}

export default App

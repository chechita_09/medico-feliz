import React from 'react'

const Header = (props) => {
  return (
    <header className="mb-4">
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
        <div className="container-fluid">
          <a className="navbar-brand ms-4 fw-bold" href="/">
            Clínica Médico Feliz
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="collapse navbar-collapse me-4"
            id="navbarSupportedContent"
          >
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0 me-4">
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="/">
                  Home
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" href="/citas">
                  Crear cita
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" href="/pacientes">
                  Registrar paciente
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" href="/consultas">
                  Consultar citas
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  )
}

export default Header

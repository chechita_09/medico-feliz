import React from 'react'

const Home = () => {
  return (
    <div className="container">
      <p className="fs-5 fw-bolder">Seleccione una opcion</p>

      <div className="container-sm mt-5 px-4">
        <div className="row row-cols-3 justify-content-around gap-4">
          <a
            className="btn btn-primary col fs-4 lh-lg p-3 w-50 align-middle"
            href="/citas"
          >
            Crear cita
          </a>

          <a
            className="btn btn-primary col fs-4 lh-lg p-3 w-50 align-middle"
            href="/pacientes"
          >
            Registrar paciente
          </a>

          <a
            className="btn btn-primary col fs-4 lh-lg p-3 w-50 align-middle"
            href="/consultas"
          >
            Consultar citas
          </a>
        </div>
      </div>
    </div>
  )
}

export default Home

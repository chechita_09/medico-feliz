import React from 'react'

const Pacientes = () => {
  return (
    <form>
      <div className="form-group mt-4">
        <label className="mb-2" for="nombrePaciente">
          Nombre
        </label>
        <input
          type="text"
          className="form-control"
          id="nombrePaciente"
          placeholder="Nombre de paciente"
        />
      </div>
      <div className="form-group mt-4">
        <label className="mb-2" for="telefonoPaciente">
          Telefono
        </label>
        <input
          type="text"
          className="form-control"
          id="telefonoPaciente"
          placeholder="Teléfono de paciente"
        />
      </div>
      <div className="form-group mt-4">
        <label className="mb-2" for="nombrePaciente">
          Dirección
        </label>
        <input
          type="text"
          className="form-control"
          id="nombrePaciente"
          placeholder="Nombre de paciente"
        />
      </div>
      <div className="form-group mt-4">
        <label className="mb-2" for="fechaCita">
          Fecha de Nacimiento
        </label>

        <input
          className="form-control w-25"
          type="datetime-local"
          id="fechaCita"
          name="fechaCita"
          value="1999-01-01T09:30"
          min="1900-01-01T00:00"
        />
      </div>
      <button type="submit" className="btn btn-primary fs-3 mt-5">
        Registrar
      </button>
    </form>
  )
}
export default Pacientes

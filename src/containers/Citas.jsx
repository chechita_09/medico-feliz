import React from 'react'

const Citas = () => {
  return (
    <form>
      <div className="form-group mt-4">
        <label className="mb-2" for="pacientes">
          Seleccione paciente
        </label>
        <select
          className="form-select"
          name="pacientes"
          aria-label="Default select example"
        >
          <option selected>Seleccione el paciente</option>
          <option value="1">Diego Gómez</option>
          <option value="2">Diana Perez</option>
          <option value="3">Ricardo Garcia</option>
          <option value="4">María Martínez</option>
          <option value="5">Juan Carlos Perez</option>
        </select>
      </div>
      <div className="form-group mt-4">
        <label className="mb-2" for="medicos">
          Seleccione médico
        </label>
        <select
          className="form-select"
          name="medicos"
          aria-label="Default select example"
        >
          <option selected>Seleccione el médico</option>
          <option value="1">Dra. Laura Gómez</option>
          <option value="2">Dr. Mario Castañeda</option>
          <option value="3">Dr. Jorge Luna</option>
          <option value="4">Dr. Josue paredes</option>
          <option value="5">Dr. Oswaldo Ordónez</option>
        </select>
      </div>
      <div className="form-group mt-4">
        <label className="mb-2" for="especialidad">
          Seleccione especialidad
        </label>
        <select
          className="form-select"
          name="especialidad"
          aria-label="Default select example"
        >
          <option selected>Seleccione la especialidad</option>
          <option value="1">Pediatría</option>
          <option value="2">Medicina General</option>
          <option value="3">Ginecología</option>
        </select>
      </div>
      <div className="form-group mt-4">
        <label className="mb-2" for="fechaCita">
          Seleccione Fecha de la cita
        </label>

        <input
          className="form-control w-50"
          type="datetime-local"
          id="fechaCita"
          name="fechaCita"
          value="2021-06-12T09:30"
          min="1900-01-01T00:00"
        />
      </div>
      <button type="submit" className="btn btn-primary mt-5 fs-5">
        Crear cita
      </button>
    </form>
  )
}

export default Citas

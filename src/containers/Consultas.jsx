import React from 'react'

const Consultas = () => {
  return (
    <div className="mt-5">
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Nombre paciente</th>
            <th scope="col">Médico</th>
            <th scope="col">Especialidad</th>
            <th scope="col">Fecha y hora</th>
            <th scope="col">&nbsp;</th>
            <th scope="col">&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Diego Gómez</td>
            <td>Dra. Laura Gómez</td>
            <td>Pediatría</td>
            <td>12/12/2021 09:30 am</td>
            <td>
              <button type="button" class="btn btn-warning">
                Editar
              </button>
            </td>
            <td>
              <button type="button" class="btn btn-danger">
                Cancelar
              </button>
            </td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Diana Perez</td>
            <td>Dra. Laura Gómez</td>
            <td>Pediatría</td>
            <td>12/12/2021 10:00 am</td>
            <td>
              <button type="button" class="btn btn-warning">
                Editar
              </button>
            </td>
            <td>
              <button type="button" class="btn btn-danger">
                Cancelar
              </button>
            </td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Ricardo Garcia</td>
            <td>Dr. Mario Castañeda</td>
            <td>Medicina General</td>
            <td>12/12/2021 09:00 am</td>
            <td>
              <button type="button" class="btn btn-warning">
                Editar
              </button>
            </td>
            <td>
              <button type="button" class="btn btn-danger">
                Cancelar
              </button>
            </td>
          </tr>
          <tr>
            <th scope="row">4</th>
            <td>María Martínez</td>
            <td>Dr. Josue paredes</td>
            <td>Ginecología</td>
            <td>12/12/2021 10:00 am</td>
            <td>
              <button type="button" class="btn btn-warning">
                Editar
              </button>
            </td>
            <td>
              <button type="button" class="btn btn-danger">
                Cancelar
              </button>
            </td>
          </tr>
          <tr>
            <th scope="row">5</th>
            <td>Juan Carlos Perez</td>
            <td>Dr. Mario Castañeda</td>
            <td>Pediatría</td>
            <td>12/12/2021 11:30 am</td>
            <td>
              <button type="button" class="btn btn-warning">
                Editar
              </button>
            </td>
            <td>
              <button type="button" class="btn btn-danger">
                Cancelar
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default Consultas
